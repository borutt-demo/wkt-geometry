package org.borutt.wkt.converter;

import java.io.IOException;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.borutt.wkt.model.Geometry;
import org.borutt.wkt.model.GeometryCollection;
import org.borutt.wkt.model.LineString;
import org.borutt.wkt.model.MultiLineString;
import org.borutt.wkt.model.MultiPoint;
import org.borutt.wkt.model.MultiPolygon;
import org.borutt.wkt.model.Point;
import org.borutt.wkt.model.Polygon;

/**
 * Transforms a WKT-formatted String into a composition of Geometry objects.
 * <p>
 * Class instance is stateless and is thread-safe.
 * </p>
 */
public class WktReader {

    private static final String POINT = "POINT";
    private static final String LINESTRING = "LINESTRING";
    private static final String POLYGON = "POLYGON";
    private static final String GEOMETRYCOLLECTION = "GEOMETRYCOLLECTION";
    private static final String MULTIPOINT = "MULTIPOINT";
    private static final String MULTILINESTRING = "MULTILINESTRING";
    private static final String MULTIPOLYGON = "MULTIPOLYGON";

    /**
     * Transforms provided WKT-formatted String into a composition of Geometry
     * objects. The WKT-fromatted string is delegated to a stream reader that is
     * further controlled by a StreamTokenizer and a WktTokenizer instance.
     * 
     * @param wktString
     *            WKT string to be parsed into geometry object composition.
     * @return Transformed geometry object
     * @throws WktInvalidTextException
     *             If malformed WKT data structure encountered.
     */
    public Geometry read(String wktString) throws WktInvalidTextException {
        try (Reader ioReader = new StringReader(wktString)) {
            WktTokenizer tokenizer = new WktTokenizer(new StreamTokenizer(ioReader));
            return readGeometryTaggedText(tokenizer);
        } catch (IOException e) {
            throw new WktIoException("Error creating IO reader for the WKT string.", e);
        }
    }

    private Geometry readGeometryTaggedText(WktTokenizer tokenizer) throws WktInvalidTextException {
        Geometry geometry = null;
        String geometryTagText = tokenizer.next().getWord();
        boolean emptyGeom = tokenizer.next().isWordEmpty();
        switch (geometryTagText) {
        case POINT:
            geometry = emptyGeom ? new Point() : readPoint(tokenizer);
            break;
        case LINESTRING:
            geometry = emptyGeom ? new LineString() : readLineString(tokenizer);
            break;
        case POLYGON:
            geometry = emptyGeom ? new Polygon() : readPolygon(tokenizer);
            break;
        case GEOMETRYCOLLECTION:
            geometry = emptyGeom ? new GeometryCollection<>() : readGeometryCollection(tokenizer);
            break;
        case MULTIPOINT:
            geometry = emptyGeom ? new MultiPoint() : readMultiPoint(tokenizer);
            break;
        case MULTILINESTRING:
            geometry = emptyGeom ? new MultiLineString() : readMultiLineString(tokenizer);
            break;
        case MULTIPOLYGON:
            geometry = emptyGeom ? new MultiPolygon() : readMultiPolygon(tokenizer);
            break;
        default:
            throw new WktInvalidTextException("Encountered reader unsupported geometry " + geometryTagText);
        }
        return geometry;
    }

    private Point readPoint(WktTokenizer tokenizer) throws WktInvalidTextException {
        tokenizer.expectLeftParenthesis();
        Point[] points = readPoints(tokenizer);
        if (points.length != 1) {
            throw new WktInvalidTextException("More than one point available.");
        }
        return points[0];
    }

    private LineString readLineString(WktTokenizer tokenizer) throws WktInvalidTextException {
        tokenizer.expectLeftParenthesis();
        Point[] points = readPoints(tokenizer);
        return new LineString(points);
    }

    private Polygon readPolygon(WktTokenizer tokenizer) throws WktInvalidTextException {
        tokenizer.expectLeftParenthesis();
        tokenizer.next().expectLeftParenthesis();
        LineString outer = readLineString(tokenizer);
        tokenizer.next();
        List<LineString> holes = new LinkedList<>();
        while (tokenizer.isComma()) {
            tokenizer.next().expectLeftParenthesis();
            holes.add(readLineString(tokenizer));
            tokenizer.next();
        }
        tokenizer.expectRightParenthesis();
        return new Polygon(outer, holes.isEmpty() ? null : holes.toArray(new LineString[holes.size()]));
    }

    private GeometryCollection<Geometry> readGeometryCollection(WktTokenizer tokenizer) throws WktInvalidTextException {
        tokenizer.expectLeftParenthesis();
        List<Geometry> geometries = new LinkedList<>();
        do {
            geometries.add(readGeometryTaggedText(tokenizer));
            // Comma or right parenthesis expected.
            tokenizer.next();
        } while (tokenizer.isComma());
        tokenizer.expectRightParenthesis();
        return new GeometryCollection<>(geometries);
    }

    private MultiPoint readMultiPoint(WktTokenizer tokenizer) throws WktInvalidTextException {
        tokenizer.expectLeftParenthesis();
        List<Point> points = new LinkedList<>();
        do {
            tokenizer.next().expectLeftParenthesis();
            points.add(readPoint(tokenizer));
            tokenizer.next();
        } while (tokenizer.isComma());
        tokenizer.expectRightParenthesis();
        return new MultiPoint(new ArrayList<>(points));
    }

    private MultiLineString readMultiLineString(WktTokenizer tokenizer) throws WktInvalidTextException {
        tokenizer.expectLeftParenthesis();
        List<LineString> lineStrings = new LinkedList<>();
        do {
            tokenizer.next().expectLeftParenthesis();
            lineStrings.add(readLineString(tokenizer));
            tokenizer.next();
        } while (tokenizer.isComma());
        tokenizer.expectRightParenthesis();
        return new MultiLineString(new ArrayList<>(lineStrings));
    }

    private MultiPolygon readMultiPolygon(WktTokenizer tokenizer) throws WktInvalidTextException {
        tokenizer.expectLeftParenthesis();
        List<Polygon> polygons = new LinkedList<>();
        do {
            tokenizer.next().expectLeftParenthesis();
            polygons.add(readPolygon(tokenizer));
            tokenizer.next();
        } while (tokenizer.isComma());
        tokenizer.expectRightParenthesis();
        return new MultiPolygon(new ArrayList<>(polygons));
    }

    /**
     * Reads a sequence of comma separated coordinate tuples "x0 y0, x1 y1, x2 y2,
     * ..." until closing parenthesis is reached.
     *
     * @param tokenizer
     * @return Sequence of points (x0, y0), (x1, y1), (x2, y2), ...
     * @throws WktInvalidTextException
     */
    private Point[] readPoints(WktTokenizer tokenizer) throws WktInvalidTextException {
        List<Point> points = new LinkedList<>();
        do {
            points.add(new Point(parseNumber(tokenizer.next().getWord()), parseNumber(tokenizer.next().getWord())));
            // Comma or right parenthesis expected. Ignores comma.
            tokenizer.next();
        } while (!tokenizer.isRightParenthesis());
        return points.toArray(new Point[points.size()]);
    }

    private double parseNumber(String strNumber) throws WktInvalidTextException {
        try {
            if (strNumber.startsWith("+")) {
                strNumber = strNumber.substring(1);
            }
            return NumberFormat.getInstance(Locale.US).parse(strNumber).doubleValue();
        } catch (ParseException e) {
            throw new WktInvalidTextException("Invalid number format for number " + strNumber, e);
        }
    }

}
