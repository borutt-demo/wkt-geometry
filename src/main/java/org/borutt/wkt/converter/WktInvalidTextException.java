package org.borutt.wkt.converter;

public class WktInvalidTextException extends Exception {

    private static final long serialVersionUID = 1L;

    public WktInvalidTextException() {
        super();
    }

    public WktInvalidTextException(String message, Throwable cause) {
        super(message, cause);
    }

    public WktInvalidTextException(String message) {
        super(message);
    }

    public WktInvalidTextException(Throwable cause) {
        super(cause);
    }

}
