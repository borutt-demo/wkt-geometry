package org.borutt.wkt.converter;

import java.io.IOException;
import java.io.StreamTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * WKT tokenizer serves as part of WKT lexer, providing control of sequentially
 * read tokens specific to WKT syntax. It depends on provided StreamTokenizer
 * instance to perform text tokenization from its wrapped string reader.
 * <p>
 * Tokens are defined as words composed of ASCII letters, digits, dots, symbols
 * minus and plus. White space characters that delimit tokens include all ASCII
 * control characters including space character. Numbers are regarded as textual
 * values.
 * </p>
 */
class WktTokenizer {

    private static final Logger logger = LoggerFactory.getLogger(WktTokenizer.class);

    private static final String EMPTY = "EMPTY";
    private static final char LEFT_PAREN = '(';
    private static final char RIGHT_PAREN = ')';
    private static final char COMMA = ',';

    private final StreamTokenizer tokenizer;

    public WktTokenizer(StreamTokenizer tokenizer) {
        this.tokenizer = tokenizer;
        init();
    }

    private void init() {
        tokenizer.resetSyntax();
        tokenizer.wordChars('a', 'z');
        tokenizer.wordChars('A', 'Z');
        tokenizer.wordChars('0', '9');
        tokenizer.wordChars('-', '-');
        tokenizer.wordChars('+', '+');
        tokenizer.wordChars('.', '.');
        tokenizer.whitespaceChars(0, ' ');
    }

    /**
     * Obtains next token. State of the obtained token does not change until the
     * next method call. Method returns this instance to allow for fluent token
     * queries.
     * 
     * @return This instance.
     * @throws IOException
     *             Error reading next token.
     */
    public WktTokenizer next() {
        try {
            tokenizer.nextToken();
            logger.debug("Current token: {}", getTokenInfo());
            return this;
        } catch (IOException e) {
            throw new WktIoException("Error reading next token.", e);
        }
    }

    public boolean hasNext() {
        return (tokenizer.ttype != StreamTokenizer.TT_EOF);
    }

    public boolean isLeftParenthesis() {
        return (tokenizer.ttype == LEFT_PAREN);
    }

    public void expectLeftParenthesis() throws WktInvalidTextException {
        if (tokenizer.ttype != LEFT_PAREN) {
            throw new WktInvalidTextException("Left parenthesis expected.");
        }
    }

    public boolean isRightParenthesis() {
        return (tokenizer.ttype == RIGHT_PAREN);
    }

    public void expectRightParenthesis() throws WktInvalidTextException {
        if (tokenizer.ttype != RIGHT_PAREN) {
            throw new WktInvalidTextException("Right parenthesis expected.");
        }
    }

    public boolean isComma() {
        return (tokenizer.ttype == COMMA);
    }

    public void expectComma() throws WktInvalidTextException {
        if (tokenizer.ttype != COMMA) {
            throw new WktInvalidTextException("Comma expected.");
        }
    }

    public String getWord() throws WktInvalidTextException {
        if (tokenizer.ttype == StreamTokenizer.TT_WORD) {
            return tokenizer.sval.toUpperCase();
        } else {
            throw new WktInvalidTextException("WKT token word was not expected at this state.");
        }
    }

    public boolean isWordEmpty() {
        return (tokenizer.ttype == StreamTokenizer.TT_WORD) && EMPTY.equalsIgnoreCase(tokenizer.sval);
    }

    private String getTokenInfo() {
        String tokenInfo = "";
        int token = tokenizer.ttype;
        switch (token) {
        case StreamTokenizer.TT_EOF:
            tokenInfo = "(End of File encountered)";
            break;

        case StreamTokenizer.TT_EOL:
            tokenInfo = "(End of Line encountered)";
            break;

        case StreamTokenizer.TT_WORD:
            tokenInfo = "(Word) '" + tokenizer.sval + "'";
            break;

        case StreamTokenizer.TT_NUMBER:
            tokenInfo = "(Number) '" + tokenizer.nval + "'";
            break;

        default:
            tokenInfo = "'" + (char) token + "'";
        }
        return tokenInfo;
    }

}