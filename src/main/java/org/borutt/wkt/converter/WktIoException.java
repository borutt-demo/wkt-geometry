package org.borutt.wkt.converter;

public class WktIoException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public WktIoException() {
        super();
    }

    public WktIoException(String message, Throwable cause) {
        super(message, cause);
    }

    public WktIoException(String message) {
        super(message);
    }

    public WktIoException(Throwable cause) {
        super(cause);
    }

}
