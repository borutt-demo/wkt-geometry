package org.borutt.wkt.converter;

import java.text.NumberFormat;
import java.util.Locale;

import org.borutt.wkt.model.Geometry;
import org.borutt.wkt.model.GeometryCollection;
import org.borutt.wkt.model.LineString;
import org.borutt.wkt.model.MultiLineString;
import org.borutt.wkt.model.MultiPoint;
import org.borutt.wkt.model.MultiPolygon;
import org.borutt.wkt.model.Point;
import org.borutt.wkt.model.Polygon;

/**
 * Transforms the input Geometry object into WKT-formatted String. For geometry
 * composition, its root Geometry object needs to be provided.
 * <p>
 * Example usage:
 * </p>
 * 
 * <pre>
 * <code>
 * new WktWriter().write(new LineString(new Point[] { new Point(30, 10), new Point(10, 30), new Point(40, 40) }));
 * // returns "LINESTRING (30 10, 10 30, 40 40)"
 * </code>
 * </pre>
 */
public class WktWriter {

    private static final String EMPTY = "EMPTY";

    /**
     * Transforms the input Geometry object into WKT-formatted String. For geometry
     * composition, the composition's root Geometry object needs to be provided.
     * 
     * @param geom
     *            Geometry object to be transformed
     * @return WKT-formatted string
     */
    public String write(Geometry geom) {
        return write(geom, true);
    }

    /**
     * Recursive writer for a geometry composition.
     * 
     * @param geom
     *            Geometry to be transformed to WKT string.
     * @param writesGeometryTag
     *            Informs child writers whether to put their geometry tag texts into
     *            the WKT string.
     * @return
     */
    private String write(Geometry geom, boolean writesGeometryTag) {
        StringBuilder sb = new StringBuilder();
        if (geom instanceof Point) {
            sb.append(writePoint((Point) geom, writesGeometryTag));
        } else if (geom instanceof LineString) {
            sb.append(writeLineString((LineString) geom, writesGeometryTag));
        } else if (geom instanceof Polygon) {
            sb.append(writePolygon((Polygon) geom, writesGeometryTag));
        } else if (geom instanceof MultiPoint) {
            sb.append("MULTIPOINT ");
            sb.append(writeCollection((MultiPoint) geom, false));
        } else if (geom instanceof MultiLineString) {
            sb.append("MULTILINESTRING ");
            sb.append(writeCollection((MultiLineString) geom, false));
        } else if (geom instanceof MultiPolygon) {
            sb.append("MULTIPOLYGON ");
            sb.append(writeCollection((MultiPolygon) geom, false));
        } else if (geom instanceof GeometryCollection) {
            sb.append("GEOMETRYCOLLECTION ");
            sb.append(writeCollection((GeometryCollection<?>) geom, true));
        }
        return sb.toString();
    }

    private StringBuilder writeCollection(GeometryCollection<?> geometries, boolean writesChildGeometryTags) {
        StringBuilder sb = new StringBuilder();
        if (geometries.isEmpty()) {
            sb.append(EMPTY);
        } else {
            sb.append('(');
            boolean firstGeometry = true;
            for (Geometry childGeom : geometries) {
                if (!firstGeometry) {
                    sb.append(", ");
                }
                sb.append(write(childGeom, writesChildGeometryTags));
                firstGeometry = false;
            }
            sb.append(')');
        }
        return sb;
    }

    private StringBuilder writePoint(Point point, boolean writesGeometryTag) {
        StringBuilder sb = new StringBuilder();
        if (writesGeometryTag) {
            sb.append("POINT ");
        }
        if (point.isEmpty()) {
            sb.append(EMPTY);
        } else {
            sb.append('(');
            sb.append(formatNumber(point.getX()));
            sb.append(' ');
            sb.append(formatNumber(point.getY()));
            sb.append(')');
        }
        return sb;
    }

    private StringBuilder writeLineString(LineString lineString, boolean writesGeometryTag) {
        StringBuilder sb = new StringBuilder();
        if (writesGeometryTag) {
            sb.append("LINESTRING ");
        }
        if (lineString.isEmpty()) {
            sb.append(EMPTY);
        } else {
            sb.append('(');
            for (int pointIdx = 0; pointIdx < lineString.getNumPoints(); pointIdx++) {
                if (pointIdx > 0) {
                    sb.append(", ");
                }
                sb.append(formatNumber(lineString.getPointN(pointIdx).getX()));
                sb.append(' ');
                sb.append(formatNumber(lineString.getPointN(pointIdx).getY()));
            }
            sb.append(')');
        }
        return sb;
    }

    private StringBuilder writePolygon(Polygon polygon, boolean writesGeometryTag) {
        StringBuilder sb = new StringBuilder();
        if (writesGeometryTag) {
            sb.append("POLYGON ");
        }
        if (polygon.isEmpty()) {
            sb.append(EMPTY);
        } else {
            sb.append('(');
            sb.append(writeLineString(polygon.getExteriorRing(), false));
            for (int interiorRingIdx = 0; interiorRingIdx < polygon.getNumInteriorRing(); interiorRingIdx++) {
                sb.append(", ");
                sb.append(writeLineString(polygon.getInteriorRingN(interiorRingIdx), false));
            }
            sb.append(')');
        }
        return sb;
    }

    private String formatNumber(double number) {
        return NumberFormat.getInstance(Locale.US).format(number);
    }
}
