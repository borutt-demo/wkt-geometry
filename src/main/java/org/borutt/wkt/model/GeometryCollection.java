package org.borutt.wkt.model;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class GeometryCollection<T extends Geometry> implements Geometry, Iterable<T> {

    private final List<T> geometries;

    public GeometryCollection() {
        geometries = Collections.emptyList();
    }

    public GeometryCollection(List<T> geometries) {
        this.geometries = geometries != null ? geometries : Collections.emptyList();
    }

    public List<T> getGeometries() {
        return geometries;
    }

    public int getNumGeometries() {
        return geometries.size();
    }

    public T getGeometryN(int geometryIndex) {
        return geometries.get(geometryIndex);
    }

    @Override
    public boolean isEmpty() {
        return geometries.isEmpty();
    }

    @Override
    public Iterator<T> iterator() {
        return geometries.iterator();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((geometries == null) ? 0 : geometries.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GeometryCollection<?> other = (GeometryCollection<?>) obj;
        if (geometries == null) {
            if (other.geometries != null) {
                return false;
            }
        } else if (!geometries.equals(other.geometries)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "GeometryCollection [geometries=" + geometries + "]";
    }

}
