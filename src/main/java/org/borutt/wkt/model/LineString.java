package org.borutt.wkt.model;

import java.util.Arrays;
import java.util.Objects;

public class LineString implements Curve {

    private final Point[] points;

    public LineString() {
        this.points = new Point[0];
    }

    public LineString(Point[] points) {
        this.points = points != null ? points : new Point[0];
    }

    public int getNumPoints() {
        return points.length;
    }

    public Point getPointN(int pointIndex) {
        return points[pointIndex];
    }

    @Override
    public boolean isEmpty() {
        return points.length == 0;
    }

    public boolean isClosed() {
        if (isEmpty()) {
            return true;
        }
        if (points.length < 2) {
            return false;
        }
        return Objects.equals(points[0], points[points.length - 1]);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(points);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LineString other = (LineString) obj;
        return Arrays.equals(points, other.points);
    }

    @Override
    public String toString() {
        return "LineString [points=" + Arrays.toString(points) + "]";
    }

}
