package org.borutt.wkt.model;

import java.util.Arrays;

public class Polygon implements Surface {

    private final LineString exteriorRing;
    private final LineString[] interiorRings;

    public Polygon() {
        exteriorRing = new LineString(); // TODO: test
        interiorRings = new LineString[0];
    }

    public Polygon(LineString exteriorRing, LineString[] interiorRings) {
        this.exteriorRing = exteriorRing != null ? exteriorRing : new LineString();
        this.interiorRings = interiorRings != null ? Arrays.copyOf(interiorRings, interiorRings.length)
                : new LineString[0];
        validateRings();
    }

    private void validateRings() {
        if (exteriorRing.isEmpty()) {
            if (interiorRings != null && interiorRings.length > 0) {
                throw new IllegalArgumentException("Empty polygon cannot have interior rings");
            }

        } else if (!exteriorRing.isClosed()) {
            throw new IllegalArgumentException("Exterior ring is not closed");
        }
        for (LineString interiorRing : interiorRings) {
            if (!interiorRing.isClosed()) {
                throw new IllegalArgumentException("Interior ring is not closed");
            }
        }
    }

    public LineString getExteriorRing() {
        return exteriorRing;
    }

    public int getNumInteriorRing() {
        return interiorRings.length;
    }

    public LineString getInteriorRingN(int interiorRingIndex) {
        return interiorRings[interiorRingIndex];
    }

    @Override
    public boolean isEmpty() {
        return exteriorRing.isEmpty();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((exteriorRing == null) ? 0 : exteriorRing.hashCode());
        result = prime * result + Arrays.hashCode(interiorRings);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Polygon other = (Polygon) obj;
        if (exteriorRing == null) {
            if (other.exteriorRing != null) {
                return false;
            }
        } else if (!exteriorRing.equals(other.exteriorRing)) {
            return false;
        }
        if (!Arrays.equals(interiorRings, other.interiorRings)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Polygon [exteriorRing=" + exteriorRing + ", interiorRings=" + Arrays.toString(interiorRings) + "]";
    }

}
