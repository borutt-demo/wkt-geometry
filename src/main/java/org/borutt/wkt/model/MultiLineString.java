package org.borutt.wkt.model;

import java.util.List;

public final class MultiLineString extends GeometryCollection<LineString> {

    public MultiLineString() {
        super();
    }

    public MultiLineString(List<LineString> geometries) {
        super(geometries);
    }

}
