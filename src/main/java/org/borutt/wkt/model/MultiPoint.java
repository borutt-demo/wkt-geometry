package org.borutt.wkt.model;

import java.util.List;

public final class MultiPoint extends GeometryCollection<Point> {

    public MultiPoint() {
        super();
    }

    public MultiPoint(List<Point> geometries) {
        super(geometries);
    }

}
