package org.borutt.wkt.model;

import java.util.List;

public final class MultiPolygon extends GeometryCollection<Polygon> {

    public MultiPolygon() {
        super();
    }

    public MultiPolygon(List<Polygon> geometries) {
        super(geometries);
    }

}
