package org.borutt.wkt.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.borutt.wkt.model.Geometry;
import org.borutt.wkt.model.GeometryCollection;
import org.borutt.wkt.model.LineString;
import org.borutt.wkt.model.MultiLineString;
import org.borutt.wkt.model.MultiPoint;
import org.borutt.wkt.model.MultiPolygon;
import org.borutt.wkt.model.Point;
import org.borutt.wkt.model.Polygon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class WktWriterTest {

    private WktWriter writer;

    @BeforeEach
    void setUp() {
        writer = new WktWriter();
    }

    private Point[] convertToPoints(double... coords) {
        Point[] points = new Point[coords.length / 2];
        for (int pointIdx = 0; pointIdx < points.length; pointIdx++) {
            points[pointIdx] = new Point(coords[pointIdx * 2], coords[pointIdx * 2 + 1]);
        }
        return points;
    }

    @Test
    void givenPoint_whenWrite_ThenWkt() {
        // GIVEN
        Geometry geometry = new Point(30, 10);
        // WHEN
        String wktResult = writer.write(geometry);
        // THEN
        assertEquals("POINT (30 10)", wktResult.toUpperCase());
    }

    @Test
    void givenPointEmpty_whenWrite_ThenWkt() {
        // GIVEN
        Geometry geometry = new Point();
        // WHEN
        String wktResult = writer.write(geometry);
        // THEN
        assertEquals("POINT EMPTY", wktResult.toUpperCase());
    }

    @Test
    void givenLineString_whenWrite_ThenWkt() {
        // GIVEN
        Geometry geometry = new LineString(convertToPoints(4, 6, 7, 10));
        // WHEN
        String wktResult = writer.write(geometry);
        // THEN
        assertEquals("LINESTRING (4 6, 7 10)", wktResult.toUpperCase());
    }

    @Test
    void givenLineStringEmpty_whenWrite_ThenWkt() {
        // GIVEN
        Geometry geometry = new LineString();
        // WHEN
        String wktResult = writer.write(geometry);
        // THEN
        assertEquals("LINESTRING EMPTY", wktResult.toUpperCase());
    }

    @Test
    void givenPolygon_whenWrite_ThenWkt() {
        // GIVEN
        LineString outer = new LineString(convertToPoints(30, 10, 40, 40, 20, 40, 10, 20, 30, 10));
        LineString[] holes = null;
        Geometry geometry = new Polygon(outer, holes);
        // WHEN
        String wktResult = writer.write(geometry);
        // THEN
        assertEquals("POLYGON ((30 10, 40 40, 20 40, 10 20, 30 10))", wktResult.toUpperCase());
    }

    @Test
    void givenPolygonWithHole_whenWrite_ThenWkt() {
        // GIVEN
        LineString outer = new LineString(convertToPoints(35, 10, 45, 45, 15, 40, 10, 20, 35, 10));
        LineString[] holes = new LineString[] { new LineString(convertToPoints(20, 30, 35, 35, 30, 20, 20, 30)) };
        Geometry geometry = new Polygon(outer, holes);
        // WHEN
        String wktResult = writer.write(geometry);
        // THEN
        assertEquals("POLYGON ((35 10, 45 45, 15 40, 10 20, 35 10), (20 30, 35 35, 30 20, 20 30))",
                wktResult.toUpperCase());
    }

    @Test
    void givenPolygonEmpty_whenWrite_ThenWkt() {
        // GIVEN
        Geometry geometry = new Polygon();
        // WHEN
        String wktResult = writer.write(geometry);
        // THEN
        assertEquals("POLYGON EMPTY", wktResult.toUpperCase());
    }

    @Test
    void givenGeometryCollectionOfPointAndLineString_whenWrite_ThenWkt() {
        // GIVEN
        Point point = new Point(4, 6);
        LineString lineString = new LineString(convertToPoints(4, 6, 7, 10));
        Geometry geometry = new GeometryCollection<Geometry>(Arrays.asList(point, lineString));
        // WHEN
        String wktResult = writer.write(geometry);
        // THEN
        assertEquals("GEOMETRYCOLLECTION (POINT (4 6), LINESTRING (4 6, 7 10))", wktResult.toUpperCase());
    }

    @Test
    void givenGeometryCollectionEmpty_whenWrite_ThenWkt() {
        // GIVEN
        Geometry geometry = new GeometryCollection<Geometry>();
        // WHEN
        String wktResult = writer.write(geometry);
        // THEN
        assertEquals("GEOMETRYCOLLECTION EMPTY", wktResult.toUpperCase());
    }

    @Test
    void givenMultiPoint_whenWrite_ThenWkt() {
        // GIVEN
        Geometry geometry = new MultiPoint(Arrays.asList(convertToPoints(10, 40, 40, 30, 20, 20, 30, 10)));
        // WHEN
        String wktResult = writer.write(geometry);
        // THEN
        assertEquals("MULTIPOINT ((10 40), (40 30), (20 20), (30 10))", wktResult.toUpperCase());
    }

    @Test
    void givenMultiPointEmpty_whenWrite_ThenWkt() {
        // GIVEN
        Geometry geometry = new MultiPoint();
        // WHEN
        String wktResult = writer.write(geometry);
        // THEN
        assertEquals("MULTIPOINT EMPTY", wktResult.toUpperCase());
    }

    @Test
    void givenMultiLineString_whenWrite_ThenWkt() {
        // GIVEN
        LineString lineString1 = new LineString(convertToPoints(10, 10, 20, 20, 10, 40));
        LineString lineString2 = new LineString(convertToPoints(40, 40, 30, 30, 40, 20, 30, 10));
        Geometry geometry = new MultiLineString(Arrays.asList(lineString1, lineString2));
        // WHEN
        String wktResult = writer.write(geometry);
        // THEN
        assertEquals("MULTILINESTRING ((10 10, 20 20, 10 40), (40 40, 30 30, 40 20, 30 10))", wktResult.toUpperCase());
    }

    @Test
    void givenMultiLineStringEmpty_whenWrite_ThenWkt() {
        // GIVEN
        Geometry geometry = new MultiLineString();
        // WHEN
        String wktResult = writer.write(geometry);
        // THEN
        assertEquals("MULTILINESTRING EMPTY", wktResult.toUpperCase());
    }

    @Test
    void givenMultiPolygon_whenWrite_ThenWkt() {
        // GIVEN
        Polygon polygon1 = new Polygon(new LineString(convertToPoints(40, 40, 20, 45, 45, 30, 40, 40)), null);
        LineString polygon2Outer = new LineString(convertToPoints(20, 35, 10, 30, 10, 10, 30, 5, 45, 20, 20, 35));
        LineString polygon2Hole = new LineString(convertToPoints(30, 20, 20, 15, 20, 25, 30, 20));
        Polygon polygon2 = new Polygon(polygon2Outer, new LineString[] { polygon2Hole });
        Geometry geometry = new MultiPolygon(Arrays.asList(polygon1, polygon2));
        // WHEN
        String wktResult = writer.write(geometry);
        // THEN
        assertEquals(
                "MULTIPOLYGON (((40 40, 20 45, 45 30, 40 40)), ((20 35, 10 30, 10 10, 30 5, 45 20, 20 35), (30 20, 20 15, 20 25, 30 20)))",
                wktResult.toUpperCase());
    }

    @Test
    void givenMultiPolygonEmpty_whenWrite_ThenWkt() {
        // GIVEN
        Geometry geometry = new MultiPolygon();
        // WHEN
        String wktResult = writer.write(geometry);
        // THEN
        assertEquals("MULTIPOLYGON EMPTY", wktResult.toUpperCase());
    }

}
