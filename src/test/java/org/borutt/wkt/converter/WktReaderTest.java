package org.borutt.wkt.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;

import org.borutt.wkt.model.Geometry;
import org.borutt.wkt.model.GeometryCollection;
import org.borutt.wkt.model.LineString;
import org.borutt.wkt.model.MultiLineString;
import org.borutt.wkt.model.MultiPoint;
import org.borutt.wkt.model.MultiPolygon;
import org.borutt.wkt.model.Point;
import org.borutt.wkt.model.Polygon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class WktReaderTest {

    private WktReader reader;

    @BeforeEach
    void setUp() {
        reader = new WktReader();
    }

    private Point[] convertToPoints(double... coords) {
        Point[] points = new Point[coords.length / 2];
        for (int pointIdx = 0; pointIdx < points.length; pointIdx++) {
            points[pointIdx] = new Point(coords[pointIdx * 2], coords[pointIdx * 2 + 1]);
        }
        return points;
    }

    @Test
    void givenWktPoint_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "POINT(30 10)";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        assertEquals(new Point(30, 10), geometry);
    }

    @Test
    void givenWktPointEmpty_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "POINT EMPTY";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        assertEquals(new Point(), geometry);
    }

    @Test
    void givenWktLineString_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "LINESTRING (4 6, 7 10)";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        assertEquals(new LineString(convertToPoints(4, 6, 7, 10)), geometry);
    }

    @Test
    void givenWktLineStringEmpty_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "LINESTRING EMPTY";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        assertEquals(new LineString(), geometry);
    }

    @Test
    void givenWktPolygon_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "POLYGON ((30 10, 40 40, 20 40, 10 20, 30 10))";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        LineString outer = new LineString(convertToPoints(30, 10, 40, 40, 20, 40, 10, 20, 30, 10));
        Geometry expectedGeometry = new Polygon(outer, null);
        assertEquals(expectedGeometry, geometry);
    }

    @Test
    void givenWktPolygonWithHole_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "POLYGON ((35 10, 45 45, 15 40, 10 20, 35 10), (20 30, 35 35, 30 20, 20 30))";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        LineString outer = new LineString(convertToPoints(35, 10, 45, 45, 15, 40, 10, 20, 35, 10));
        LineString[] holes = new LineString[] { new LineString(convertToPoints(20, 30, 35, 35, 30, 20, 20, 30)) };
        Geometry expectedGeometry = new Polygon(outer, holes);
        assertEquals(expectedGeometry, geometry);
    }

    @Test
    void givenWktPolygonEmpty_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "POLYGON EMPTY";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        assertEquals(new Polygon(), geometry);
    }

    @Test
    void givenWktGeometryCollectionOfPointAndLineString_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "GEOMETRYCOLLECTION (POINT (4 6), LINESTRING (4 6, 7 10))";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        Point point = new Point(4, 6);
        LineString lineString = new LineString(convertToPoints(4, 6, 7, 10));
        Geometry expectedGeometry = new GeometryCollection<Geometry>(Arrays.asList(point, lineString));
        assertEquals(expectedGeometry, geometry);
    }

    @Test
    void givenWktGeometryCollectionEmpty_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "GEOMETRYCOLLECTION EMPTY";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        assertEquals(new GeometryCollection<Geometry>(), geometry);
    }

    @Test
    void givenWktMultiPoint_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "MULTIPOINT ((10 40), (40 30), (20 20), (30 10))";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        Geometry expectedGeometry = new MultiPoint(Arrays.asList(convertToPoints(10, 40, 40, 30, 20, 20, 30, 10)));
        assertEquals(expectedGeometry, geometry);
    }

    @Test
    void givenWktMultiPointEmpty_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "MULTIPOINT EMPTY";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        assertEquals(new MultiPoint(), geometry);
    }

    @Test
    void givenWktMultiLineString_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "MULTILINESTRING ((10 10, 20 20, 10 40), (40 40, 30 30, 40 20, 30 10))";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        LineString lineString1 = new LineString(convertToPoints(10, 10, 20, 20, 10, 40));
        LineString lineString2 = new LineString(convertToPoints(40, 40, 30, 30, 40, 20, 30, 10));
        Geometry expectedGeometry = new MultiLineString(Arrays.asList(lineString1, lineString2));
        assertEquals(expectedGeometry, geometry);
    }

    @Test
    void givenWktMultiLineStringEmpty_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "MULTILINESTRING EMPTY";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        assertEquals(new MultiLineString(), geometry);
    }

    @Test
    void givenWktMultiPolygon_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "MULTIPOLYGON (((40 40, 20 45, 45 30, 40 40)), ((20 35, 10 30, 10 10, 30 5, 45 20, 20 35), (30 20, 20 15, 20 25, 30 20)))";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        Polygon polygon1 = new Polygon(new LineString(convertToPoints(40, 40, 20, 45, 45, 30, 40, 40)), null);
        LineString polygon2Outer = new LineString(convertToPoints(20, 35, 10, 30, 10, 10, 30, 5, 45, 20, 20, 35));
        LineString polygon2Hole = new LineString(convertToPoints(30, 20, 20, 15, 20, 25, 30, 20));
        Polygon polygon2 = new Polygon(polygon2Outer, new LineString[] { polygon2Hole });
        Geometry expectedGeometry = new MultiPolygon(Arrays.asList(polygon1, polygon2));
        assertEquals(expectedGeometry, geometry);
    }

    @Test
    void givenWktMultiPolygonEmpty_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "MULTIPOLYGON EMPTY";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        assertEquals(new MultiPolygon(), geometry);
    }

    @Test
    void givenWktPointScientificNumbers_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "POINT(+.3 -2.3456e-3)";
        // WHEN
        Geometry geometry = reader.read(wkt);
        // THEN
        assertEquals(new Point(.3, -2.3456e-3), geometry);
    }

    @Test
    void givenInvalidWktPointNumbers_whenRead_ThenGeometry() throws WktInvalidTextException {
        // GIVEN
        String wkt = "POINT(K2 7)";
        // THEN
        assertThrows(WktInvalidTextException.class, () -> {
            // WHEN
            reader.read(wkt);
        });
    }

    @Test
    void givenInvalidWktPointWithThreeCoordinates_whenRead_ThenWktInvalidTextException()
            throws WktInvalidTextException {
        // GIVEN
        String wkt = "POINT(2 3 4)";
        // THEN
        assertThrows(WktInvalidTextException.class, () -> {
            // WHEN
            reader.read(wkt);
        });
    }

    @Test
    void givenInvalidWktPointWithTwoCoordinateTuples_whenRead_ThenWktInvalidTextException()
            throws WktInvalidTextException {
        // GIVEN
        String wkt = "POINT(2 3, 4 5)";
        // THEN
        assertThrows(WktInvalidTextException.class, () -> {
            // WHEN
            reader.read(wkt);
        });
    }

    @Test
    void givenUnsupportedWktGeometryText_whenRead_ThenWktInvalidTextException() throws WktInvalidTextException {
        // GIVEN
        String wkt = "TRIANGLE EMPTY";
        // THEN
        assertThrows(WktInvalidTextException.class, () -> {
            // WHEN
            reader.read(wkt);
        });
    }
}
