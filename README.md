# Well-known Text Representation for Geometry reader and writer

The project contains Java source code for conversion of geometry objects into Well-known Text (WKT), and the opposite. WKT and its grammar is defined by Open Geospatial Consortium (OGC).

Please follw the following sources for detailed information about WKT:
* [Wikipedia](http://en.wikipedia.org/wiki/Well-known_text) - general overview
* [OGC Simple Feature Access specifications - Simple Feature Access - Part 1: Common Architecture](http://www.opengeospatial.org/standards/sfa)
  * Specification v1.2.1: [OpenGIS Implementation Specification for Geographic information - Simple feature access - Part 1: Common architecture](http://portal.opengeospatial.org/files/?artifact_id=25355) - refer particularly to the chapter 7 of the document

## Implementation specifics

Implemention is split into a WKT reader and a WKT writer. Reader performs conversion of a WKT-formatted string into a geometry object. A geometry object may be composed of several other geometry objects. Writer performs conversion of a geometry object or a geometry objects composition (the root geometry object should be provided to the writer) into a WKT-formatted string.

In contrast to the above specification the code implements only a subset of the specified WKT definitions and differs is some aspect.

Only 2D geometry objects are supported (POINT Z, POINT M, POINT ZM are excluded).

Following geometries are supported by the WKT reader and the writer implementation:
* Point
* LineString
* Polygon
* GeometryCollection
* MultiPoint
* MultiLineString
* MultiPolygon


## Getting Started
Wrappers for build tools Maven and Gradle are provided for convenience - no manual installation of these tools is required. Use wrapper command `mvnw` for Maven and `gradlew` for Gradle.

JDK 1.8 is required.

## Building and Installing

### Maven

Build code and package it into a JAR artifact:

    $ ./mvnw clean package

> Created JAR artifacts with built code, Javadoc and source code are available in subdirectory `./target`.

Installtion of built code into the local repository for reuse as dependency in other projects:

    $ ./mvnw clean install

### Gradle

Build code and package it into a JAR artifact:

    $ ./gradlew clean build

> Created JAR artifacts with built code, Javadoc and source code are available in subdirectory `./build/libs`.

## Running the tests

### Maven

    $ ./mvnw clean test

Create a tests report:

    $ ./mvnw surefire-report:report  

> Generated tests report: `./target/site/surefire-report.html`

### Gradle
    $ ./gradlew clean test
    
> Generated tests report: `./build/reports/tests/test/index.html`

## Authors
* Borut Turšič

## Acknowledgments

* [JTS Topology Suite - WKT reader/writer implementation](https://github.com/simplegeo/jts/tree/master/src/com/vividsolutions/jts/io)  - inspiration for the WKT writer implementation